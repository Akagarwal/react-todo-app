import React, { Component } from "react";


const filters = [
    {
        label: "All",
        value: "all"
    },
    {
        label: "Active",
        value: "active"
    }, {
        label: "Completed",
        value: "completed"
    },
]

class Footer extends Component {

    render() {
        return (
            <footer className="bottom-items flex-row">
                <div className="items-left"><span id="items-remaining">{this.props.totalItems}</span> items left</div>
                <div className="filter flex-row">

                    {filters.map((filter) => {
                        return <span
                            key={filter.label}
                            className="filters"
                            onClick={() => this.props.handleSetFilter(filter)}>
                            {filter.label}</span>;
                    })}
                    {/* <label> */}
                    {/* <input type="radio" name="filter" id="all" /> */}
                    {/* <span className="filters" onClick={this.props.filterAll}>All</span> */}
                    {/* </label> */}

                    {/* <label> */}
                    {/* <input type="radio" name="filter" id="active" /> */}
                    {/* <span className="filters" onClick={this.props.filterActive}>Active</span> */}
                    {/* </label> */}

                    {/* <label> */}
                    {/* <input type="radio" name="filter" id="completed" /> */}
                    {/* <span className="filters" onClick={this.props.filterCompleted}>Completed</span> */}
                    {/* </label> */}
                </div>
                <span onClick={this.props.rmCompleted} className="clear">Clear Completed</span>
            </footer>
        );
    }
}

export default Footer;