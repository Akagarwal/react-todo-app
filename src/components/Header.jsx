import React, { Component } from "react";

class Header extends Component {
    state = {}
    render() {
        return (
            <header className="flex-row">
                <h1 id="main-heading">TODO</h1>
                <div className="theme-block">
                    <input type="checkbox" id="theme" />
                    <label onClick={this.props.themeChange} htmlFor="theme"></label>
                </div>
            </header>
        );
    }
}

export default Header;