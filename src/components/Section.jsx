import React, { Component } from "react";

class Section extends Component {

    render() {
        console.log(this.props.toCreateTodo);
        return (
            <section className="content">
                <ul>

                    {this.props.toCreateTodo.map((task) => {
                        return (
                            <li id={task.id} className="flex-row">
                                <label className="list-items">
                                    <input
                                        id="ul-li-checked"
                                        type="checkbox"
                                        checked={task.completed}
                                        onChange={() => this.props.toChangeMark(task.id)}>
                                    </input>
                                    <span className="checkmark"></span>
                                    <span className="text">{task.title}</span>
                                </label>
                                <span
                                    className="remove"
                                    onClick={() => this.props.toRemove(task.id)}>
                                </span>
                            </li>
                        )
                    })}
                </ul>
            </section>);
    }
}

export default Section;
