import './App.css';
import React, { Component } from 'react';
import Header from './components/Header';
import Section from './components/Section';
import Footer from './components/Footer';

class App extends Component {

  state = {
    todos: [],
    activeFilter: "all",
    themeClass: "theme-dark body"
  }
  themeChanger = () => {
    this.state.themeClass === "theme-dark body" ? this.setState({ themeClass: "theme-light body" }) : this.setState({ themeClass: "theme-dark body" })
  }

  handleInput = (e) => {
    if (e.key === "Enter") {
      let todosInput = e.target.value;
      e.target.value = ""
      if (todosInput.length !== 0) {
        let todo = {
          id: new Date(),
          title: todosInput,
          completed: false,
        }
        let newListItem = [...this.state.todos, todo]
        this.setState({ todos: newListItem });
      }
    }
  }

  markComplete = (itemId) => {
    this.setState({
      todos: this.state.todos.map((todo) => {
        if (todo.id === itemId) {
          return {
            ...todo,
            completed: !todo.completed,
          };
        }
        return todo;
      })
    })
    // let newTodo = this.state.todos.map((todo) => {
    //   if (todo.id === itemId) {
    //     todo.completed = !todo.completed;
    //   }
    //   return todo
    // })
    // this.setState({ todos: newTodo })
  }

  removeTodo = (itemId) => {
    this.setState({
      todos: this.state.todos.filter(todo => todo.id !== itemId)
    })
    // let newTodo = this.state.todos.filter((todo) => {
    //   return todo.id !== itemId
    // });
    // this.setState({ todos: newTodo })
  }

  removeCompleted = () => {
    let newTodo = this.state.todos.filter((obj) => obj.completed === false);
    this.setState({ todos: newTodo })
  }

  // showAll = () => {
  //   let array = this.state.todos;
  //   let todos = array.map(obj => {
  //     obj.display = true
  //     return obj
  //   })
  //   this.setState({ todos: todos })
  // }

  // showActive = () => {
  //   let array = this.state.todos;
  //   let todos = array.map((obj) => {
  //     if (obj.completed === true) {
  //       obj.display = false
  //     } else {
  //       obj.display = true
  //     }
  //     return obj
  //   });
  //   this.setState({ todos: todos })
  // }

  // showCompleted = () => {
  //   let array = this.state.todos;
  //   let todos = array.map(obj => {
  //     if (obj.completed === true) {
  //       obj.display = true
  //     } else {
  //       obj.display = false
  //     }
  //     return obj
  //   });
  //   this.setState({ todos: todos })
  // }

  getFilteredTodos = () => {
    const activeFilter = this.state.activeFilter;
    if (activeFilter === "all") {
      console.log("hello");
      return this.state.todos;
    } else if (activeFilter === "completed") {
      return this.state.todos.filter((todo) => todo.completed)
    } else if (activeFilter === "active") {
      return this.state.todos.filter((todo) => !todo.completed)
    }
    else {
      return []
    }
  }

  handleSetFilter = (filter) => {
    this.setState({
      activeFilter: filter.value
    })
  }

  render() {
    const displayFiltered = this.getFilteredTodos();
    return (
      <div className={this.state.themeClass}>
        <Header themeChange={this.themeChanger} />
        <main>
          <div className="add-new-item flex-row">
            <span></span>
            <input
              onKeyUp={this.handleInput}
              type="text"
              placeholder="Create New Todo" id="additem"
            />
          </div>
          <Section
            toCreateTodo={displayFiltered}
            // displayFiltered={this.toCreateTodo}
            toChangeMark={this.markComplete}
            toRemove={this.removeTodo}
          />
          <Footer
            totalItems={this.state.todos.filter((obj) => obj.completed === false).length}
            rmCompleted={this.removeCompleted}
            // filterAll={this.showAll}
            // filterActive={this.showActive}
            // filterCompleted={this.showCompleted}
            handleSetFilter={this.handleSetFilter}
          />
        </main>
      </div>
    );
  }
}

export default App;
